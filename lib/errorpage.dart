import 'common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class ErrorPage extends StatelessWidget{
  const ErrorPage({
    Key key,
    @required this.errorString,
}) : super(key: key);

  final String errorString;
  static const routeName = '/errorpage';

  @override
  Widget build(BuildContext context){
    final ThemeData theme = Theme.of(context);
    final TextStyle big = theme.textTheme.title;
      return new Scaffold(
        body: new Center(
          child: new ListView(
            children: <Widget>[
              new Container(
                padding: const EdgeInsets.all(32.0),
                child: new Card(
                  elevation: 10.0,
                  child: new Container(
                    padding: const EdgeInsets.all(8.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Text(
                          "Hiba",
                          style: big,
                        ),
                        new Container(
                          padding: const EdgeInsets.all(8.0),
                          child: new Icon(Icons.error),
                        ),
                        new Text(errorString),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
  }
}
import '../common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:firebase_database/firebase_database.dart';

class InfoPage extends StatefulWidget {
  @override
  _InfoPageState createState() => new _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {

  var infoString;
  
  @override
  void initState(){
    super.initState();
    Future<Null> load() async {
      var ref = FirebaseDatabase.instance.reference().child("infos");
      var snapshot = await ref.once();
      setState((){
        infoString = snapshot.value;
      });
    }
    load();
  }
  
  @override
  Widget build(BuildContext context){

    return new Center(
      child: new ListView(
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: infoString == null
                    ? new Center(child: new CircularProgressIndicator(),)
                    : new Text(infoString),
              ),
            ),
          )
        ],
      ),
    );
  }
}
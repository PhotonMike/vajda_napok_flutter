import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../login/details.dart';
import '../common.dart';
import '../pages.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'startbackground.dart';
import '../main.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class StartPage extends StatefulWidget {
  const StartPage({
    Key key,
    @required this.person,
  }
    ) : super(key: key);
  static const String routeName = '/content/startpage';
  final Person person;

  @override
  _StartPageState createState() => new _StartPageState();
}
class _StartPageState extends State<StartPage> with TickerProviderStateMixin {

  Widget content;
  int _content;

  //Begin ad stuff.
  static const String appId = 'ca-app-pub-6736529284382840~7029717839';
  //static const String testDevice = 'B65C499519A0DE76B132E7779549C17C';
  static const String testDevice = null;
  static const String bannerAdUnitId = 'ca-app-pub-6736529284382840/9081166109';

  static final MobileAdTargetingInfo targetingInfo = new MobileAdTargetingInfo(
    testDevices: testDevice != null ? <String>[testDevice] : null,
    keywords: <String>['school', 'high school'],
  );

  BannerAd _bannerAd;

  BannerAd createBannerAd() {
    return new BannerAd(
      unitId: bannerAdUnitId,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        print("BannerAd event $event");
      },
    );
  }
  //End ad stuff.

  static String title;

  static Person _person = new Person();

  bool scoresEnabled = false;
  bool gameEnabled = false;
  final enablestuff = FirebaseDatabase.instance.reference().child("features");

  static final database = FirebaseDatabase.instance;
  static final users = FirebaseDatabase.instance.reference().child('users');
  static final enableApp = database.reference().child('enable');

  Future<Null> refreshUI() async {
    await _person.resolve();
    await person.resolve();
    if (_person.name == null || _person.userClass == null){
      Navigator.pushReplacementNamed(context, DetailsPage.routeName);
    }
    final DataSnapshot appEnable = await enableApp.once();
    if (_person.userClass != "11A" && appEnable.value == "disabled"){
      VajdaNapok.throwError("Alkalmazás tesztelés alatt. Nézz vissza később ;)");
      Navigator.pushReplacementNamed(context, ErrorPage.routeName);
    }
    if (appEnable.value == "totallydisabled"){
      VajdaNapok.throwError("Alkalmazás letiltva.");
      Navigator.pushReplacementNamed(context, ErrorPage.routeName);
    }
    final DataSnapshot enables = await enablestuff.once();
    final HashMap hashMap = enables.value;
    scoresEnabled = hashMap["scores"];
    gameEnabled = hashMap["game"];
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    refreshUI();
    FirebaseAdMob.instance.initialize(appId: appId);
    _bannerAd = createBannerAd()
      ..load()
      ..show();
  }

  @override
  void dispose() {
    _bannerAd.dispose();
    super.dispose();
  }

  void launchWeb(String url) {
    final sheet = new FlutterWebviewPlugin();
    sheet.launch(url);
  }

  @override
  Widget build(BuildContext context) {

    var tStyle = Theme.of(context).textTheme.body1.copyWith(color: Colors.white);

    List<PageContent> pages = <PageContent>[
      new PageContent(
        title: "Kezdőlap",
        color: Colors.teal[300],
        content: new StartPageContent(person: widget.person,),
      ),
      new PageContent(
        title: "A Gomb.",
        color: Colors.pink[200],
        content: new TheButton(person: widget.person),
      ),
      new PageContent(
        title: "Információk",
        color: Colors.cyan[400],
        content: new InfoPage(),
      ),
      new PageContent(
        title: "A verseny állása",
        color: Colors.teal[300],
        content: scoresEnabled
            ? null
            : new NotYet(),
        fn: scoresEnabled
            ? (){launchWeb("https://docs.google.com/spreadsheets/d/1XzZoXFpwBUUi9k0p5kyYsXuCI_ClTbvgnt_JqmaTUHQ/edit?usp=sharing");}
            : null,
      ),
      new PageContent(
        title: "Játék",
        color: Colors.pink[200],
        content: gameEnabled
            ? null
            : new NotYet(),
        fn: scoresEnabled
            ? (){launchWeb("https://vajdanapok-78eee.firebaseapp.com");}
            : null,
      ),
      new PageContent(
        title: "Közösség",
        color: Colors.cyan[400],
        content: new CommunityPage(),
      ),
      new PageContent(
        title: "Támogatóink",
        color: Colors.grey[400],
        content: new SponsorsPage(person: widget.person,),
      )
    ];

    if (person.userClass == "11A" && (person.isAdmin ?? false)) {
      pages.add(new PageContent(
        title: "Admin felület",
        color: Colors.grey[900],
        content: new AdminPage(),
      ));
    }

    List<OtherActions> actions = <OtherActions>[
      new OtherActions(
        title: "Adatok szerkesztése",
        onTap: (){
          _bannerAd.dispose();
          Navigator.pushReplacementNamed(context, DetailsPage.editRoute);
        },
      ),
      new OtherActions(
        title: "Kijelentkezés",
        onTap: (){
          Future<Null> signOut() async {
            await widget.person.mAuth.signOut();
            var cs = new CommonSettings();
            (await cs.emailFile()).delete();
            (await cs.passwordFile()).delete();
            Navigator.pushReplacementNamed(context, LoginPage.routeName);
          }
          signOut();
        },
      )
    ];

    int i=0;

    if (content == null) {
      content = pages[0]._content;
      title = pages[0]._title;
      _content = 0;
    }

    return new Scaffold(
        appBar: new AppBar(
          title: new Text(title),
        ),
        drawer: new Drawer(
          child: new ListView(
            children: <Widget>[
              new ClipRect(
                child: new Image(image: new AssetImage('assets/banner.png')),
              ),
              new ClipRect(
                child: new Column(
                  children: <Widget>[]
                    ..addAll(pages.map((PageContent pageContent){
                      final int sel = i;
                      i++;
                      return new Container(
                        color: pageContent._color,
                        child: new ListTile(
                          title: new Text(pageContent._title, style: tStyle,),
                          selected: _content == sel,
                          onTap: pageContent._fn ?? (){
                            if (pageContent._content != null){
                              setState((){
                                _content = sel;
                                title = pageContent._title;
                                content = pageContent._content;
                              });
                              Navigator.pop(context);
                            }
                          },
                        ),
                      );
                    }).toList())
                    ..add(const Divider(
                      height: 32.0,
                    ))
                    ..addAll(actions.map((OtherActions action){
                      return new ListTile(
                        dense: true,
                        title: new Text(action._title),
                        onTap: action._onTap,
                      );
                    }).toList())
                    ..add(new Container(
                      padding: const EdgeInsets.only(bottom: 60.0),
                      child: new Divider(
                        height: 10.0,
                      ),
                    )),
                ),
              ),
            ],
          ),
        ),
        body: new AnimatedContainer(
          duration: const Duration(seconds: 3),
          child: content,
          curve: Curves.fastOutSlowIn,
        ),
    );
  }
}

class PageContent{
  PageContent({
    String title,
    Widget content,
    Color color,
    GestureTapCallback fn,
  }): _title = title,
      _content = content,
      _color = color,
      _fn = fn;

  final String _title;
  final Widget _content;
  final Color _color;
  final GestureTapCallback _fn;
}

class OtherActions{
  OtherActions({
    String title,
    GestureTapCallback onTap,
  }): _title = title,
      _onTap = onTap;

  final String _title;
  final GestureTapCallback _onTap;
}
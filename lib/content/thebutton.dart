import '../common.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'dart:collection';

class TheButton extends StatefulWidget {
  const TheButton({
    Key key,
    @required this.person,
  }
  ) : super(key: key);

  final Person person;
  static const String routeName = '/content/thebutton';

  @override
  _TheButtonState createState() => new _TheButtonState();
}

class _TheButtonState extends State<TheButton> {

  bool pushable = false;

  static final database = FirebaseDatabase.instance.reference();
  final classicButtonDB = database.child("minigames");
  final userDB = database.child("users");

  bool running;
  bool loaded = false;
  bool syncOldStuff = false;

  static const String appId = 'ca-app-pub-6736529284382840~7029717839';
  static const String testDevice = 'B65C499519A0DE76B132E7779549C17C';
  static const String interstitialAdUnitId = 'ca-app-pub-6736529284382840/7474217184';
  static final MobileAdTargetingInfo targetingInfo = new MobileAdTargetingInfo(
    testDevices: testDevice != null ? <String>[testDevice] : null,
    keywords: <String>['school', 'high school'],
  );

  int localButtonPushes = 0;

  String userClass;

  final buttondb = database.child("minigames").child("thebutton");
  DatabaseReference cButton;
  StreamSubscription<Event> buttonSub;

  @override
  initState(){
    super.initState();

    FirebaseAdMob.instance.initialize(appId: appId);

    userClass = widget.person.userClass;
    FirebaseDatabase.instance.setPersistenceEnabled(true);
    FirebaseDatabase.instance.setPersistenceCacheSizeBytes(10000000);
    cButton = FirebaseDatabase.instance.reference().child("minigames").child("thebutton").child(userClass);
    userDB.keepSynced(true);
    pushable = false;
    loaded = false;
    scoreLoad();
    buttonSub = userDB.onValue.listen((Event e) {
      userD = e.snapshot.value;
      assembleScores();
      setState((){});
    });
    running = true;
  }

  Future<Null> addToButton() async{
    pushable = false;
    localButtonPushes++;
    try{
      await userDB.child(widget.person.account.uid).child("thebutton").set(localButtonPushes);
    } catch(e) {
      await new Future.delayed(const Duration(milliseconds: 100));
    }

    assembleScores();
    setState((){
      pushable = true;
    });
  }

  List<String> classes;
  List<Widget> secondCard;
  LinkedHashMap<String, int> scores;
  LinkedHashMap userD;
  LinkedHashMap classicD;
  List<int> dummy = <int>[];

  Future<Null> scoreLoad() async {
    var dBCall = await database.once();
    LinkedHashMap hashMap = dBCall.value;
    hashMap.forEach((key, value){
      if (key == "minigames") {
        LinkedHashMap hm = value;
        hm.forEach((key, value){
          if (key == "thebutton") {
            classicD = value;
          }
        });
      } else if (key == "users") {
        userD = value;
      } else if (key == "versions"){
        LinkedHashMap hm = value;
        syncOldStuff = hm["1;8"];
      }
    });
    classes = <String>[];
    classicD.forEach((key, value){
      classes.add(key);
      dummy.add(0);
    });
    assembleScores();
    setState((){
      loaded = true;
      pushable = true;
    });
  }

  void assembleScores() {
    var tempScores = new Map.fromIterables(classes, dummy);
    classicD.forEach((key, value){
      tempScores[key] = int.parse(value) ?? 0;
    });
    userD.forEach((key, value){
      LinkedHashMap hm = value;
      String curClass;
      int curButton = 0;
      bool isLoggedInUser = (widget.person.account.uid == key);
      hm.forEach((key, value){
        if (key == "class")
          curClass = value;
        if (key == "thebutton"){
          curButton = value;
          if (isLoggedInUser)
            localButtonPushes = value;
        }
      });
      tempScores[curClass] += curButton;
    });
    scores = sort(tempScores);
  }

  LinkedHashMap<String, int> sort(Map<String, int> sortThis){
    var sortedKeys = sortThis.keys.toList(growable: false)
      ..sort((k1, k2) => sortThis[k2].compareTo(sortThis[k1]));
    LinkedHashMap sortedMap = new LinkedHashMap
        .fromIterable(sortedKeys, key: (k) => k, value: (k) => sortThis[k]);
    return sortedMap;
  }

  @override
  void dispose(){
    running = false;
    buttonSub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    secondCard = <Widget>[];

    if (loaded){
      double maximum = 0.0;
      scores.forEach((key, value){
        if (value.toDouble() > maximum)
          maximum = value.toDouble();
      });
      for (int i = 0; i<classes.length; i++){
        var curClass = scores.keys.toList()[i];
        var score = scores[curClass];
        double percentToMax = score/maximum;
        secondCard.add(new Container(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: new Column(
            children: <Widget>[
              new Text(curClass+": "+score.toString()),
              new LinearProgressIndicator(value: percentToMax,),
            ],
          ),
        ));
      }
    } else {
      secondCard = <Widget>[
        new CircularProgressIndicator(),
      ];
    }

    return new Center(
      child: new ListView(
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Text(widget.person.userClass),
                    new IconButton(
                      icon: pushable
                          ? new Icon(Icons.add)
                          : new CircularProgressIndicator(),
                      onPressed: pushable
                          ? (){
                          setState((){
                            pushable = false;
                          });
                          addToButton();
                      }
                          :null,
                    ),
                    new Text(userClass == null || !loaded
                        ? "Betöltés..."
                        : "A Gomb megnyomva eddig ${scores[userClass]} alkalommal a $userClass által.",
                    ),
                  ],
                ),
              ),
            ),
          ),
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: secondCard,
                ),
              ),
            ),
          ),
          new Container(
            padding: const EdgeInsets.all(40.0),
          ),
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';

class CommunityPage extends StatelessWidget{

  Future<Null> launchURL(String url) async{
    if (await canLaunch(url))
      await launch(url);
  }

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new ListView(
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: new Column(
                  children: <Widget>[
                    new IconButton(
                        icon: new Image(image: new AssetImage('assets/FB.png')),
                        iconSize: 150.0,
                        onPressed: (){
                          launchURL("https://www.facebook.com/VajdaNapok/");
                        },
                    )
                  ],
                ),
              ),
            ),
          ),
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: new Column(
                  children: <Widget>[
                    new IconButton(
                      icon: new Image(image: new AssetImage('assets/Insta.png')),
                      iconSize: 150.0,
                      onPressed: (){
                        launchURL("https://www.instagram.com/explore/tags/vn30/");
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
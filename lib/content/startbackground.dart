import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import '../common.dart';

class NewsItem {
  NewsItem({
    String title,
    DateTime start,
    DateTime end,
    String uid,
  }): _title = title,
      _start = start,
      _end = end,
      _uid = uid;

  final String _title;
  final DateTime _start;
  final DateTime _end;
  final String _uid;
}

class StartPageContent extends StatefulWidget {
  const StartPageContent({
    Key key,
    @required this.person,
}) : super(key: key);

  final Person person;

  @override
  _StartPageContentState createState() => new _StartPageContentState();
}

class _StartPageContentState extends State<StartPageContent> {
  
  List<Widget> newsWidgetList = <Widget>[];

  List<NewsItem> newsList = <NewsItem>[];
  
  bool running;
  
  final ref = FirebaseDatabase.instance.reference().child("news");

  double progressValue;
  static final DateTime sept = new DateTime(2017, 09, 1, 8);
  static DateTime now;
  static final DateTime kezd = new DateTime(2017, 09, 14, 12, 20);

  var septToKezd = kezd.difference(sept);
  static var remaining;

  var days;
  var hours;
  var mins;
  var secs;

  Future<Null> loadFromDatabase() async {
    DataSnapshot load = await ref.once();
    LinkedHashMap hm = load.value;
    newsList = <NewsItem>[];
    hm.forEach((key, value){
      LinkedHashMap hashMap = value;
      String title = hashMap["title"];
      String start = hashMap["startDate"];
      String end = hashMap["endDate"];
      newsList.add(
          new NewsItem(
            title: title,
            start: DateTime.parse(start),
            end: DateTime.parse(end),
            uid: key,
          )
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
    running = false;
  }

  @override
  void initState(){
    super.initState();
    running = true;
    Future<Null> refreshUi() async {
      loadFromDatabase();
      var i = 0;
      while (running) {
        i++;
        if (running)
          setState((){});
        if (i%10 == 9)
          loadFromDatabase();
        await new Future.delayed(const Duration(seconds: 1));
      }
    }
    refreshUi();
  }

  @override
  Widget build(BuildContext context){

    final ThemeData theme = Theme.of(context);
    final TextStyle titleStyle = theme.textTheme.headline.copyWith();

    SplayTreeMap<int, NewsItem> sortedNews = new SplayTreeMap();
    newsList.forEach((NewsItem news){
      var start = news._start;
      Duration dur = start.difference(new DateTime.now());
      while (sortedNews.containsKey(dur.inSeconds)) {
        start = start.add(new Duration(seconds: 1));
        dur = start.difference(new DateTime.now());
      }
      sortedNews.addAll(<int, NewsItem>{
        dur.inSeconds: news
      });
    });

    newsList = sortedNews.values.toList();

    newsWidgetList = newsList.map((NewsItem news){
      bool going = false;
      String uid = news._uid;
      var end = septToKezd;
      DateTime start;
      var kezd = news._start;
      now = new DateTime.now();
      remaining = kezd.difference(now);

      if (remaining.isNegative){
        start = kezd;
        kezd = news._end;
        going = true;
        remaining = kezd.difference(now);
      }

      if (remaining.isNegative){
        return new Container();
      }

      if (going)
        end = kezd.difference(start);
      progressValue = (remaining.inSeconds)/(end.inSeconds);

      days = remaining.inDays;
      hours = remaining.inHours - days*24;
      mins = remaining.inMinutes - remaining.inHours*60;
      secs = remaining.inSeconds - remaining.inMinutes*60;

      return new Container(
        padding: const EdgeInsets.all(16.0),
        child: new Card(
          elevation: 10.0,
          child: new Container(
            padding: const EdgeInsets.all(8.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      news._title,
                      style: titleStyle,
                      textAlign: TextAlign.left,
                    ),
                    widget.person.isAdmin
                        ? new IconButton(
                            icon: const Icon(Icons.remove_circle_outline),
                            onPressed: (){
                              ref.child(uid).remove();
                            }
                          )
                        : new Container()
                    ,
                  ],
                ),
                new Divider(height: 8.0,),
                new Text(going
                    ? 'Idő a végéig:'
                    : 'Idő a kezdésig:'
                ),
                new Container(
                  padding: const EdgeInsets.symmetric(vertical: 4.0),
                  child: new Text('$days nap, $hours óra, $mins perc, $secs másodperc'),
                ),
                new LinearProgressIndicator(
                  value: progressValue,
                ),
              ],
            ),
          ),
        ),
      );
    }).toList();

    newsWidgetList.add(
        new Container(
          padding: const EdgeInsets.all(30.0),
        ));

    return new Center(
      child: new ListView(
        children: newsWidgetList.length == 0
            ? <Widget>[new Container(
          padding: const EdgeInsets.all(32.0),
          child: new Center(child: new CircularProgressIndicator(),),
        )]
            : newsWidgetList,
      ),
    );
  }
}
import 'package:flutter/material.dart';
import '../common.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:collection';
import 'package:flutter/foundation.dart';
import 'package:firebase_storage/firebase_storage.dart';

class Sponsor {
  Sponsor({
    this.URL,
    this.imageURL,
    this.money,
    this.uid,
});
  final String URL, imageURL, uid;
  final int money;

}

class SponsorsPage extends StatefulWidget {
  const SponsorsPage({
    Key key,
    @required this.person,
}) : super(key: key);

  final Person person;

  @override
  _SponsorsPageState createState() => new _SponsorsPageState();
}

class _SponsorsPageState extends State<SponsorsPage> {

  List<Sponsor> sponsors = <Sponsor>[];
  List<int> mon = <int>[];
  SplayTreeMap<int, Sponsor> splayTreeMap;
  List<Widget> sponsorCards = <Widget>[];

  Future<Null> launchURL(String url) async{
    if (await canLaunch(url))
      await launch(url);
  }

  final sponsorsDB = FirebaseDatabase.instance.reference().child("sponsors");
  final sponsorImgs = FirebaseStorage.instance.ref().child("sponsors");

  Future<Null> loadFromDB() async {
    sponsors = <Sponsor>[];
    mon = <int>[];
    DataSnapshot ds = await sponsorsDB.once();
    LinkedHashMap hm = ds.value;
    hm.forEach((key, value){
      String URL, imageURL;
      String uid = key;
      int money;
      LinkedHashMap hm = value;
      URL = hm["URL"];
      imageURL = hm["imageURL"];
      money = hm["money"];
      while (mon.contains(money))
        money--;
      mon.add(money);
      sponsors.add(new Sponsor(
        URL: URL,
        imageURL: imageURL,
        money: money,
        uid: uid,
      ));
    });
    splayTreeMap = new SplayTreeMap.fromIterables(mon, sponsors);
    setState((){});
  }

  @override
  void initState(){
    super.initState();
    loadFromDB();
  }

  @override
  Widget build(BuildContext context) {

    sponsorCards = <Widget>[];

    splayTreeMap?.forEach((key, value){
      sponsorCards.add(
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: new Column(
                  children: <Widget>[
                    new IconButton(
                        icon: new Image.network(value.imageURL),
                        iconSize: 100.0,
                        onPressed: (){
                          launchURL(value.URL);
                        }
                    ),
                    widget.person.isAdmin
                        ? new IconButton(
                        icon: const Icon(Icons.remove_circle_outline),
                        onPressed: (){
                          sponsorsDB.child(value.uid).remove();
                        })
                        : new Container()
                  ],
                )
              ),
            ),
          )
      );
    });

    sponsorCards = sponsorCards.reversed.toList();
    sponsorCards.add(
        new Container(
          padding: const EdgeInsets.all(32.0),
        ));

    return new Center(
      child: new ListView(
        children: sponsorCards.length <= 1
            ? <Widget>[new Container(
                padding: const EdgeInsets.all(32.0),
                child: new Center(child: new CircularProgressIndicator(),),
        )]
            : sponsorCards,
      ),
    );
  }
}
import 'package:flutter/material.dart';

class NotYet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new ListView(
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: new Text("Ezen az oldalon a Vajda Napok kezdetével jelenik meg tartalom."),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
import 'dart:math';

import '../common.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

class AdminPage extends StatefulWidget {
  @override
  _AdminPageState createState() => new _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {

  final TextEditingController _startTitle = new TextEditingController();
  final TextEditingController _sponsorURL = new TextEditingController();
  final TextEditingController _donation = new TextEditingController();
  final TextEditingController _info = new TextEditingController();

  final sponsorDB = FirebaseDatabase.instance.reference().child("sponsors");
  final infodb = FirebaseDatabase.instance.reference().child("infos");

  File imageFile;
  bool loading = false;
  bool uploaded = false;
  String imageURL;


  DateTime startDate;
  TimeOfDay startTime;
  DateTime endDate;
  TimeOfDay endTime;

  DateTime selDate;
  TimeOfDay selTime;

  Future<Null> load() async {
    DataSnapshot snap = await infodb.once();
    String infos = snap.value;
    _info.text = infos;
  }

  @override
  void initState(){
    super.initState();
    load();
  }

  @override
  Widget build(BuildContext context){

    Future<Null> pickTime() async {
      final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: new TimeOfDay(
            hour: 8,
            minute: 0),
      );
      selTime = picked;
    }

    Future<Null> pickDate() async {
      final DateTime picked = await showDatePicker(
          context: context,
          initialDate: new DateTime(2017, 09, 14),
          firstDate: new DateTime(2017, 09, 01),
          lastDate: new DateTime(2020)
      );
      selDate = picked;
      await pickTime();
    }

    Future<Null> startDatePicker() async {
      await pickDate();
      startDate = selDate;
      startTime = selTime;
      startDate = new DateTime(selDate.year, selDate.month, selDate.day, selTime.hour, selTime.minute);
      setState((){});
    }

    Future<Null> endDatePicker() async {
      await pickDate();
      endDate = selDate;
      endTime = selTime;
      endDate = new DateTime(selDate.year, selDate.month, selDate.day, selTime.hour, selTime.minute);
      setState((){});
    }

    final ThemeData theme = Theme.of(context);
    final TextStyle titleStyle = theme.textTheme.headline.copyWith();

    return new Center(
      child: new ListView(
        children: <Widget>[
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      "Főoldal cucc hozzáadása",
                      style: titleStyle,
                    ),
                    new Divider(),
                    new TextField(
                      controller: _startTitle,
                      decoration: new InputDecoration(
                        labelText: "Cím",
                      ),
                    ),
                    new FlatButton(
                        onPressed: (){
                          startDatePicker();
                        },
                        child: new Text("Válassz kezdési dátumot"),
                    ),
                    new Text(startDate == null || startTime == null
                        ? "Válassz kezdési időpontot"
                        : "${startDate.month}.${startDate.day}, ${startTime.hour}:${startTime.minute}"
                    ),
                    new Divider(),
                    new FlatButton(
                        onPressed: (){
                          endDatePicker();
                        },
                        child: new Text("Válassz befejezési időpontot.")
                    ),
                    new Text(endDate == null || endTime == null
                        ? "Válassz kezdési időpontot"
                        : "${endDate.month}.${endDate.day}, ${endTime.hour}:${endTime.minute}"
                    ),
                    new Center(
                      child: new Container(
                        padding: const EdgeInsets.all(8.0),
                        child: new RaisedButton(
                          child: new Text("Elküld"),
                          onPressed: () {
                            final ref = FirebaseDatabase.instance.reference()
                                .child("news");
                            ref.push().set(<String, String>{
                              "title": _startTitle.text,
                              "startDate": "${startDate.toIso8601String()}",
                              "endDate": "${endDate.toIso8601String()}",
                            });
                          }
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      "Sponsor hozzáadása",
                      style: titleStyle,),
                    new Divider(),
                    new TextField(
                      controller: _sponsorURL,
                      decoration: new InputDecoration(
                        labelText: "Weblap (kell a http:// az elejére!)",
                      ),
                    ),
                    new TextField(
                      controller: _donation,
                      decoration: new InputDecoration(
                        labelText: "Adomány mennyisége forintban (ne írj a végére forintot vagy ft-t!)",
                      ),
                    ),
                    new RaisedButton(
                        child: new Text("Kép csatolása"),
                        onPressed: () async {
                          try {
                            imageFile = await ImagePicker.pickImage();
                            setState((){
                              loading = true;
                            });
                            int random = new Random().nextInt(100000);
                            StorageReference ref = FirebaseStorage.instance.ref().child("sponsors").child("logo_$random.jpg");
                            StorageUploadTask uploadTask = ref.put(imageFile);
                            Uri downloadUrl = (await uploadTask.future).downloadUrl;
                            imageURL = downloadUrl.toString();
                            setState((){
                              loading = false;
                            });
                          } catch (e) {
                            loading = false;
                            imageFile = null;
                            imageURL = null;
                          }
                        }
                    ),
                    new RaisedButton(
                        child: new Text("Küldés"),
                        onPressed: (imageFile == null) || loading
                            ? null
                            : () async {
                          setState((){
                            loading = true;
                          });
                          await sponsorDB.push().set({
                            'URL': _sponsorURL.text,
                            'imageURL': imageURL,
                            'money': int.parse(_donation.text)
                          });
                          setState((){
                            loading = false;
                          });
                        }
                    ),
                    loading
                        ? new CircularProgressIndicator()
                        : new Container()
                  ],
                ),
              ),
            ),
          ),
          new Container(
            padding: const EdgeInsets.all(16.0),
            child: new Card(
              elevation: 10.0,
              child: new Container(
                padding: const EdgeInsets.all(8.0),
                child: new Column(
                  children: <Widget>[
                    new TextField(
                      controller: _info,
                      decoration: new InputDecoration(
                        labelText: "Infók",
                      ),
                    ),
                    new RaisedButton(
                        onPressed: (){
                          infodb.set(_info.text);
                        }
                    )
                  ],
                ),
              ),
            ),
          ),
          new Container(
            padding: const EdgeInsets.all(40.0),
          ),
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'pages.dart';
import 'common.dart';
import 'dart:io';
import 'dart:developer';
import 'package:flutter/foundation.dart';

FirebaseAuth auth = FirebaseAuth.instance;
Person person = new Person();

void main() {
  runApp(new VajdaNapok());
}

String errorString;

class VajdaNapok extends StatelessWidget {

  static void throwError(String error) {
    errorString = error;
  }

  @override
  Widget build(BuildContext context){
    return new MaterialApp(
      title: "Vajda Napok",
      theme: new ThemeData(
        primarySwatch: new CommonSettings().color,
        brightness:  new CommonSettings().brightness,
      ),
      home: new Load(loggedIn: false,),
      routes: <String, WidgetBuilder>{
        '/loggedin': (BuildContext context) => new Load(loggedIn: true,),
        StartPage.routeName: (BuildContext context) => new StartPage(person: person,),
        LoginPage.routeName: (BuildContext context) => new LoginPage(),
        DetailsPage.routeName: (BuildContext context) => new DetailsPage(person: person, editMode: false,),
        DetailsPage.editRoute: (BuildContext context) => new DetailsPage(person: person, editMode: true,),
        ErrorPage.routeName: (BuildContext context) => new ErrorPage(errorString: errorString,)
      },
    );
  }
}

class Load extends StatefulWidget{
  const Load({
    Key key,
    @required this.loggedIn,}
    ) : super(key: key);

  final bool loggedIn;

  @override
  _LoadState createState() => new _LoadState();
}

class _LoadState extends State<Load> {


  String goTo;

  Future<Null> loginFromFile() async {

    if(widget.loggedIn){
      person.refreshAccount();
      goTo = StartPage.routeName;
    } else {
      try {
        var cs = new CommonSettings();
        File email = await cs.emailFile();
        File password = await cs.passwordFile();
        if ((await email.exists()) && (await password.exists())) {
          try {
            await auth.signInWithEmailAndPassword(
                email: await email.readAsString(),
                password: await password.readAsString());
            await person.resolve();
            goTo = StartPage.routeName;
          } catch (e) {
            errorString = "Nem sikerült bejelentkezni. Ellenőrizd internetkapcsolatod.";
            goTo = ErrorPage.routeName;
          }
        } else {
          goTo = LoginPage.routeName;
        }
      } catch (e) {}
    }

    if (goTo != null && goTo != '/')
      Navigator.pushReplacementNamed(context, goTo);
  }

  @override
  void initState(){
    super.initState();
    loginFromFile();
  }

  @override
  Widget build(BuildContext context) {

    return new DecoratedBox(
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage('assets/logo.png')
            )
        )
    );
  }
}

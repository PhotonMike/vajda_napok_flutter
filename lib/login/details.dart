import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../common.dart';
import 'package:vajda_napok_flutter/content/startpage.dart';

class DetailsPage extends StatefulWidget {
  const DetailsPage({
    Key key,
    @required this.editMode,
    @required this.person,
  }) : super(key: key);

  static const routeName = '/login/detailspage';
  static const editRoute = routeName + 'edit';

  final bool editMode;
  final Person person;

  @override
  _DetailsPageState createState() => new _DetailsPageState();
}
class _DetailsPageState extends State<DetailsPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final acdb = FirebaseDatabase.instance.reference().child('accesscodes');
  static String AC;
  Person person = new Person();
  List<String> classes = CommonSettings.classes;
  Widget _class = new Container();
  Widget name = new Container();
  Widget accesscode = new Container();
  String uname, uclass, selClass = "9A";
  bool nextPage = true;
  MaterialColor _color = new CommonSettings().color;

  Future<String> _getAC(String mclass) async {
    final _AC = await acdb.child(mclass).once();
    AC = _AC.value;
    return _AC.value;
  }

  Future<Null> _updateUI() async {
    await person.resolve();

    if (widget.editMode)
      nextPage = false;

    if (person.name == null)
      nextPage = false;

    if (person.userClass == null)
      nextPage = false;

    if (nextPage) {
      await person.resolve();
      Navigator.pushReplacementNamed(context, StartPage.routeName);
    } else {
      setState(() {
        nextPage = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _updateUI();
  }

  Future<bool> _needToFill() async {

    String titleText = widget.editMode
        ? 'Visszalépsz?'
        : 'Kötelező kitölteni';
    String contentText = widget.editMode
        ? 'Ha visszalépsz nem lesznek ezek az adatok mentve'
        : 'Ha most kilépsz, következő indításkor kell ezeket az adatokat megadnod.';
    String exitText = widget.editMode
        ? 'Visszalépés'
        : 'Kilépés';
    VoidCallback exitAction = widget.editMode
        ? (){
      Navigator.of(context).pop(false);
      Navigator.of(context).pushReplacementNamed(StartPage.routeName);
    }
        : (){
      Navigator.of(context).pop(true);
    };

    return await showDialog<bool>(
        context: context,
        child: new AlertDialog(
          title: new Text(titleText),
          content: new Text(contentText),
          actions: <Widget>[
            new FlatButton(
                onPressed: exitAction,
                child: new Text(exitText)
            ),
            new FlatButton(
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
                child: const Text('Maradás')
            ),
          ],
        ),
    ) ?? false;
  }

  void saveUser() {
    if (uname.isNotEmpty)
      person.setName(uname);
    if (uclass.isNotEmpty)
      person.setClass(uclass);
    Navigator.pushReplacementNamed(context, StartPage.routeName);
  }

  Widget build(BuildContext context) {

    name = new TextFormField(
      decoration: const InputDecoration(
        icon: const Icon(Icons.person),
        hintText: 'Név',
        labelText: 'Név',
      ),
      validator: (String value) {
        if (value.isEmpty)
          return "Kötelező nevet megadni";
        if (value.contains(".") || value.contains("@") || value.contains("\$"))
          return "Hibás név formátum";
        return null;
      },
      onSaved: (String text) {
        uname = text;
      },
      keyboardType: TextInputType.text,
    );

    _class = new InputDecorator(
      decoration: const InputDecoration(
        labelText: 'Osztály',
        hintText: 'Válaszd ki osztályodat',
      ),
      isEmpty: selClass == null,
      child: new DropdownButton<String>(
        value: selClass,
        isDense: true,
        onChanged: (String value) {
          setState(() {
            selClass = value;
            _getAC(selClass);
          });
        },
        items: classes.map((String value) {
          return new DropdownMenuItem<String>(
            value: value,
            child: new Text(value),
          );
        }).toList(),
      ),
    );

    accesscode = new TextFormField(
      decoration: const InputDecoration(
        icon: const Icon(Icons.vpn_key),
        hintText: 'Osztály kód',
        labelText: 'Osztály kód',
      ),
      validator: (String value) {
        if (value != AC)
          return "Hibás kód.";
        if (selClass == null)
          return "Válaszd ki az osztályod";
        return null;
      },
      onSaved: (String text) {
        if (text == AC) {
          uclass = selClass;
        }
      },
    );

    Form form = new Form(
        key: _formKey,
        onWillPop: _needToFill,
        child: new ListView(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          children: <Widget>[
            name,
            accesscode,
            _class,
          ],
        ));

    var loading = new Center(child: new Text("Betöltés..."),);

    Widget _body() {
      if (nextPage)
        return loading;
      else
        return form;
    }

    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          title: const Text('További adatok'),
        ),
        body: _body(),
        floatingActionButton: new FloatingActionButton(
            child: const Icon(Icons.arrow_right),
            backgroundColor: _color,
            onPressed: () {
              final FormState form = _formKey.currentState;
              if (form.validate()) {
                form.save();
                saveUser();
                person.resolve();
              }
            },
        ),
    );
  }
}
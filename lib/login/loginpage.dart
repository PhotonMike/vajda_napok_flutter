import 'package:flutter/material.dart';
import 'details.dart';
import '../common.dart';
import 'dart:io';
import 'package:flutter/foundation.dart';

class NavigationIconView {
  NavigationIconView({
    Widget icon,
    Widget title,
    Color color,
    TickerProvider vsync,
  }) : _icon = icon,
        _color = color,
        item = new BottomNavigationBarItem(
          icon: icon,
          title: title,
          backgroundColor: color,
        ),
        controller = new AnimationController(
          duration: kThemeAnimationDuration,
          vsync: vsync,
        ) {
    _animation = new CurvedAnimation(
      parent: controller,
      curve: const Interval(0.5, 1.0, curve: Curves.fastOutSlowIn),
    );
  }

  final Widget _icon;
  final Color _color;
  final BottomNavigationBarItem item;
  final AnimationController controller;
  CurvedAnimation _animation;

  FadeTransition transition(BottomNavigationBarType type, BuildContext context) {
    Color iconColor;
    if (type == BottomNavigationBarType.shifting) {
      iconColor = _color;
    } else {
      final ThemeData themeData = Theme.of(context);
      iconColor = themeData.brightness == Brightness.light
          ? themeData.primaryColor
          : themeData.accentColor;
    }

    return new FadeTransition(
      opacity: _animation,
      child: new SlideTransition(
        position: new FractionalOffsetTween(
          begin: const FractionalOffset(0.0, 0.02), // Small offset from the top.
          end: FractionalOffset.topLeft,
        ).animate(_animation),
        child: new IconTheme(
          data: new IconThemeData(
            color: iconColor,
            size: 120.0,
          ),
          child: _icon,
        ),
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({
    Key key}
      ) : super(key: key);

  static const routeName = '/login/loginpage';

  @override
  _LoginPageState createState() => new _LoginPageState();
}
class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
  static int _currentIndex = 0;
  static final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  BottomNavigationBarType _type = BottomNavigationBarType.fixed;
  List<NavigationIconView> _navigationViews;
  static FirebaseAuth auth = FirebaseAuth.instance;
  static String email="", password="";
  static bool _autovalidate = false;
  static final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  static void _snack(String value) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(value)
    ));
  }

  @override
  void initState() {
    super.initState();
    _navigationViews = <NavigationIconView>[
      new NavigationIconView(
        icon: const Icon(Icons.account_circle),
        title: const Text('Bejelentkezés'),
        color: new CommonSettings().color,
        vsync: this,
      ),
      new NavigationIconView(
        icon: const Icon(Icons.add_circle_outline),
        title: const Text('Regisztráció'),
        color: new CommonSettings().color,
        vsync: this,
      )
    ];
    for (NavigationIconView view in _navigationViews)
      view.controller.addListener(_rebuild);
    _navigationViews[_currentIndex].controller.value = 1.0;
  }

  @override
  void dispose() {
    for (NavigationIconView view in _navigationViews)
      view.controller.dispose();
    super.dispose();
  }

  void _rebuild() {
    setState(() {
      // Rebuild in order to animate views.
    });
  }

  static String _validateEmail(String value) {
    if (value.isEmpty)
      return "Kötelező e-mail megadni";
    if (!value.contains("@") || !value.contains("."))
      return "Hibás e-mail cím";
    return null;
  }

  static TextFormField _email = new TextFormField(
    decoration: const InputDecoration(
      icon: const Icon(Icons.email),
      hintText: 'E-mail',
      labelText: 'E-mail',
    ),
    validator: _validateEmail,
    onSaved: (String text) {
      email = text;
    },
    keyboardType: TextInputType.emailAddress,
  );

  static String _validatePassword(String value) {
    if (value.isEmpty)
      return "Kötelező jelszavat megadni";
    if (value.length < 6)
      return "Minimum 6 karakter kötelező";
    return null;
  }
  
  static TextFormField _password = new TextFormField(
    decoration: const InputDecoration(
      icon: const Icon(Icons.vpn_key),
      hintText: 'Jelszó',
      labelText: 'Jelszó',
    ),
    validator: _validatePassword,
    onSaved: (String text) {
      password = text;
    },
    keyboardType: TextInputType.text,
    obscureText: true,
  );

  static Form _form = new Form(
      child: new ListView(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        children: <Widget>[
          _email,
          _password,
        ],
      ),
    key: _formKey,
    autovalidate: _autovalidate,
  );

  static Future<Null> _saveLogin(String emailStr, String passwordStr) async {
    try {
      var cs = new CommonSettings();
      File email = await cs.emailFile();
      File password = await cs.passwordFile();
      await email.writeAsString(emailStr);
      await password.writeAsString(passwordStr);
    } catch (e) {
      _snack("Mentési hiba");
    }
  }

  @override
  Widget build(BuildContext context) {

    Future<Null> _loginSignup() async {
      if (_currentIndex == 1) {
        try {
          _snack("Regisztráció folyamatban");
          await auth.createUserWithEmailAndPassword(email: email, password: password);
          await _saveLogin(email, password);
          Navigator.pushReplacementNamed(context, DetailsPage.routeName);
        } catch (e) {
          _snack("Regisztráció hiba");
        }
      } else {
        try {
          _snack("Bejelentkezés folyamatban...");
          await auth.signInWithEmailAndPassword(email: email, password: password);
          await _saveLogin(email, password);
          Navigator.pushReplacementNamed(context, DetailsPage.routeName);
        } catch (e) {
          _snack("Bejelentkezési hiba");
        }
      }
    }

    final BottomNavigationBar botNavBar = new BottomNavigationBar(
      items: _navigationViews
          .map((NavigationIconView navigationView) => navigationView.item)
          .toList(),
      currentIndex: _currentIndex,
      type: _type,
      onTap: (int index) {
        setState(() {
          _currentIndex = index;
        });
      },
    );


    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: const Text('Bejelentkezés'),
      ),
      body: _form,
      bottomNavigationBar: botNavBar,
      floatingActionButton: new FloatingActionButton(
          child: const Icon(Icons.arrow_right),
          backgroundColor: _navigationViews[_currentIndex]._color,
          onPressed: () {
            final FormState form = _formKey.currentState;
            if (form.validate()) {
                form.save();
                _loginSignup();
            } else {
              _autovalidate = true;
            }
          }),
    );
  }

}
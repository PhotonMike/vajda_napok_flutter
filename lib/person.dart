import 'dart:collection';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:async';
export 'package:firebase_database/firebase_database.dart';
export 'package:firebase_auth/firebase_auth.dart';
export 'dart:async';

class Person {
  static var auth = FirebaseAuth.instance;
  static FirebaseUser user = auth.currentUser;
  var mAuth = auth;
  FirebaseUser account = user;
  bool isAdmin;
  final userdb = FirebaseDatabase.instance.reference().child('users').child(user.uid);

  Future<Null> refreshAccount() async {
    auth = FirebaseAuth.instance;
    user = auth.currentUser;
  }

  String name = "";
  void setName(String _name) {
    userdb.child("name").set(_name);
  }
  String userClass = "";
  void setClass(String _class) {
    userdb.child("class").set(_class);
  }

  Future<Null> resolve() async {
    DataSnapshot snap = await userdb.once();
    LinkedHashMap hm = snap.value;
    hm.forEach((key, value){
      if (key == "name")
        name = value;
      if (key == "class")
        userClass = value;
      if (key == "admin")
        isAdmin = (value ?? false);
    });
  }
}
import 'package:flutter/material.dart';
export 'person.dart';
import 'dart:math';
import 'dart:io';
import 'dart:async';
import 'package:path_provider/path_provider.dart';

class CommonSettings {
  Brightness brightness = Brightness.light;
  static List<String> classes = <String>[
    "9A",
    "9B",
    "9C",
    "10A",
    "10B",
    "10C",
    "11A",
    "11B",
    "11C",
    "12A",
    "12B",
    "12C",
  ];

  static String version = "1;9";

  static var random1 = new Random().nextInt(17);
  MaterialColor color = Colors.cyan;

  static var random2 = new Random().nextInt(17);
  var secColor = Colors.primaries[random2];

  Future<File> emailFile () async {
    try {
      String dir = (await getApplicationDocumentsDirectory()).path;
      return new File('$dir/email');
    } catch (e) {
      return null;
    }
  }

  Future<File> passwordFile () async {
    try {
      String dir = (await getApplicationDocumentsDirectory()).path;
      return new File('$dir/password');
    } catch (e) {
      return null;
    }
  }
}